var config = {
  map: {
      '*': {
          'mbs_compare_products': 'Mbs_ProductCompare/js/comparesidebar',
          'mbs_product_list': 'Mbs_ProductCompare/js/productlist',
          'mbs_product_compare': 'Mbs_ProductCompare/js/productcompare',
          'mbs_compare_state':'Mbs_ProductCompare/js/state',
      }
  },
  config: {
    mixins: {
        'Magento_Catalog/js/view/compare-products': {
            'Mbs_ProductCompare/js/mixin/compare-product-sidebar': true
        }
    }
  }

};