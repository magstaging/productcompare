define([
    'jquery',
    'mbs_compare_state',
], function ($, state) {
    'use strict';

    return function (compareSidebar) {
        return compareSidebar.extend({
            default: {
                track: {
                    notsavedCompareProduct: true
                }
            },
            notsavedCompareProduct: function () {
                return state.productIdsToCompare;
            },
            getProductIdsToCompare: function() {
                var result = [];

                for (var i=0;i<state.productIdsToCompare.length; i++) {
                    result.push(state.productIdsToCompare[i].productId);
                }

                return result;
            },
            refreshNotsavedCompareProduct: function () {
                var self = this;

                $.ajax({
                    type: 'post',
                    url: this.refreshurl,
                    dataType: 'json',
                    data: {
                        product_ids: this.getProductIdsToCompare(),
                        form_key: $.cookie('form_key')
                    },
                    cache: false,

                    /** @inheritdoc */
                    beforeSend: function () {
                        $('body').trigger('processStart');
                    },
                    /** @inheritdoc */
                    success: function (res) {
                        console.log('success');
                        var eventData, parameters;

                        $('body').trigger('processStop');

                        location.reload();
                    }
                });

                return '';
            }
        });
    }

});