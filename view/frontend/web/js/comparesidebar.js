define([
    'uiComponent',
    'mbs_compare_state'
], function (Component, state) {
    'use strict';

    return Component.extend({
        defaults: {
            title: 'Product to compare (not saved)',
            template: 'Mbs_ProductCompare/comparesidebar',
        },
        productIdsToCompare: function () {
            return  state.productIdsToCompare;
        }
    });
});