define([
    'uiComponent',
    'mbs_compare_state'
], function (Component, state) {
    'use strict';

    return Component.extend({
        defaults: {
            checked: false,
            title: 'Compare checkbox',
            template: 'Mbs_ProductCompare/compare',
            tracks: {
                checked: true
            }
        },
        active: function() {

        },
        disactive: function() {

        },
        compareStatus: function () {
            if (this.getSelectedIndex()===false) {
                return this.addMessage();
            } else {
                return this.addedMessage();
            }
        },
        addedMessage: function () {
            return 'Added to compare'
        },
        addMessage: function () {
            return 'Add to compare'
        },
        getSelectedIndex: function() {
            var found = false;

            for (var i=0;i<state.productIdsToCompare.length; i++) {
                if (state.productIdsToCompare[i].productId === this.productId) {
                    found = i;
                }
            }

            return found;
        },
        addProductToCompare: function () {
            var result = [];
            var found = this.getSelectedIndex();

            if (found === false) {
                state.productIdsToCompare.push({
                    'productId':this.productId,
                    'productName': this.productName
                });
                this.checked = true;
            } else {
                state.productIdsToCompare.splice(found, 1);
                this.checked = false;
            }
            return true;
        }
    });
});