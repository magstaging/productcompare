<?php

namespace Mbs\ProductCompare\Controller\Refresh;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Data\Form\FormKey\Validator;

class Index extends \Magento\Framework\App\Action\Action implements HttpPostActionInterface
{
    /**
     * @var \Magento\Framework\Controller\ResultFactory
     */
    private $pageFactory;
    /**
     * @var \Mbs\ProductCompare\Model\CompareSidebarProductHandler
     */
    private $compareSidebarProductHandler;
    /**
     * @var Validator
     */
    private $formKeyValidator;

    public function __construct(
        Context $context,
        \Magento\Framework\Controller\ResultFactory $pageFactory,
        \Mbs\ProductCompare\Model\CompareSidebarProductHandler $compareSidebarProductHandler,
        Validator $formKeyValidator
    ) {
        parent::__construct($context);
        $this->pageFactory = $pageFactory;
        $this->compareSidebarProductHandler = $compareSidebarProductHandler;
        $this->formKeyValidator = $formKeyValidator;
    }

    /**
     * @inheritDoc
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        if (!$this->formKeyValidator->validate($this->getRequest())) {
            return $resultRedirect->setRefererUrl();
        }

        $productIds = $this->getRequest()->getParam('product_ids');

        $this->compareSidebarProductHandler->addProductListToComparisonWidget($productIds);

        $this->messageManager->addComplexSuccessMessage(
            'addCompareSuccessMessage',
            [
                'product_name' => sprintf('%s Product(s)', count($productIds)),
                'compare_list_url' => $this->_url->getUrl('catalog/product_compare'),
            ]
        );

        $result = [
            'success' => true
        ];

        $this->getResponse()->representJson(
            $this->_objectManager->get(\Magento\Framework\Json\Helper\Data::class)->jsonEncode($result)
        );
    }
}
