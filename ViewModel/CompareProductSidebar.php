<?php

namespace Mbs\ProductCompare\ViewModel;

use Magento\Framework\View\Element\Block\ArgumentInterface;

class CompareProductSidebar implements ArgumentInterface
{
    /**
     * @var \Magento\Framework\UrlInterface
     */
    private $urlBuilder;

    public function __construct(
        \Magento\Framework\UrlInterface $urlBuilder
    ) {
        $this->urlBuilder = $urlBuilder;
    }

    public function getPostDataRefreshList()
    {
        return $this->urlBuilder->getUrl('mbscompare/refresh');
    }
}
