<?php

namespace Mbs\ProductCompare\ViewModel;

use Magento\Catalog\Model\ResourceModel\Eav\Attribute;
use Magento\Catalog\Model\ResourceModel\Product\Compare\Item\Collection;
use Magento\Eav\Model\Entity\Attribute\Group;
use Magento\Framework\View\Element\Block\ArgumentInterface;

class CompareProductListing implements ArgumentInterface
{
    /**
     * @var \Mbs\ProductCompare\Model\ProductAttributeGroupsFinder
     */
    private $attributeGroupsFinder;
    /**
     * @var \Mbs\ProductCompare\Model\ProductAttributesFilter
     */
    private $productAttributesFilter;
    private $attributesListInGroup = [];

    public function __construct(
        \Mbs\ProductCompare\Model\ProductAttributeGroupsFinder $attributeGroupsFinder,
        \Mbs\ProductCompare\Model\ProductAttributesFilter $productAttributesFilter
    ) {
        $this->attributeGroupsFinder = $attributeGroupsFinder;
        $this->productAttributesFilter = $productAttributesFilter;
    }

    /**
     * @param Collection $product
     * @return array
     */
    public function getAttributeGroups(Collection $products, $attributes)
    {
        $result = [];

        $groups = $this->attributeGroupsFinder->findGroupListForProduct($products, $attributes);

        foreach ($groups as $group) {
            if (!$this->hasAttributesInGroup($products, $group, $attributes)) {
                continue;
            }

            $result[] = $group;
        }

        return $result;
    }

    /**
     * @param Collection $products
     * @param Group $group
     * @param $attributes
     * @return array
     */
    public function getAttributesInGroup(Collection $products, Group $group, $attributes)
    {
        if (!isset($this->attributesListInGroup[$group->getAttributeGroupId()])) {
            $attributes = $this->productAttributesFilter->getAttributesListInGroup(
                $group->getAttributeGroupId(),
                $attributes
            );

            $result = [];
            foreach ($attributes as $attribute) {
                if ($this->productAttributesFilter->hasItemsNonEmptyAttributeValues($attribute, $products)) {
                    $result[] = $attribute;
                }
            }

            $this->attributesListInGroup[
            $group->getAttributeGroupId()
            ] = $result;
        }

        return $this->attributesListInGroup[$group->getAttributeGroupId()];
    }

    /**
     * @param Collection $products
     * @param Group $group
     * @param $attributes
     * @return bool
     */
    public function hasAttributesInGroup(Collection $products, Group $group, $attributes)
    {
        $attributes = $this->getAttributesInGroup($products, $group, $attributes);

        if (count($attributes) > 0) {
            $numberDifferentAttributeItemValues = 0;
            foreach ($attributes as $attribute) {
                $numberDifferentAttributeItemValues += $this->productAttributesFilter->countDifferentItemsAttributeValues($attribute, $products);
            }

            return $numberDifferentAttributeItemValues >= 1;
        }

        return false;
    }

    /**
     * @param Attribute $attribute
     * @param Collection $items
     * @return bool
     */
    public function getAttributeValueIdenticalInAllItems(Attribute $attribute, Collection $items)
    {
        $numberDifferentAttributeItemValues = $this->productAttributesFilter->countDifferentItemsAttributeValues($attribute, $items);

        return $numberDifferentAttributeItemValues <= 1;
    }
}
