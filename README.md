# README #

ProductCompare enhancement extracting the product attributes groups and rendering the product compare listing separating the attributes by groups.

### How do I get set up? ###

1. clone the repository
2. create folder app/code/Mbs/ProductCompare when located at the root of the Magento site
3. copy the content of this repository within the folder
4. install the module php bin/magento setup:upgrade

add product to your compare list and verify the attributes that are setup to be comparable in the system are now rendered by groups.

This module takes over the default Magento compare list template. If a theme is enabled, it may be necessary to adjust the theme accordingly.
