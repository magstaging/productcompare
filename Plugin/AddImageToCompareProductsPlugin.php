<?php

namespace Mbs\ProductCompare\Plugin;

use Magento\Catalog\CustomerData\CompareProducts;
use Mbs\ProductCompare\Model\ProductImageExtractor;

class AddImageToCompareProductsPlugin
{
    /**
     * @var ProductImageExtractor
     */
    private $productImageExtractor;

    public function __construct(
        ProductImageExtractor $productImageExtractor
    ) {
        $this->productImageExtractor = $productImageExtractor;
    }

    public function afterGetSectionData(CompareProducts $subject, $result)
    {
        if (!empty($result['items'])) {
            $collection = $this->productImageExtractor->getProductCollectionWithImage(
                $this->getProductIds($result['items'])
            );

            foreach ($result['items'] as $i => $item) {
                $product = $collection->getItemById($item['id']);
                $result['items'][$i]['image'] = $product->getData('image');
            }
        }

        return $result;
    }

    private function getProductIds($items)
    {
        $result = [];

        foreach ($items as $item) {
            $result[] = $item['id'];
        }

        return $result;
    }
}
