<?php

namespace Mbs\ProductCompare\Model;

use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\SearchCriteriaBuilder;

class ProductImageExtractor
{
    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    private $productRepository;
    /**
     * @var FilterBuilder
     */
    private $filterBuilder;
    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;
    /**
     * @var \Magento\Framework\Data\CollectionFactory
     */
    private $collectionFactory;
    /**
     * @var \Magento\Framework\DataObjectFactory
     */
    private $dataObjectFactory;
    /**
     * @var \Magento\Catalog\Block\Product\ImageBuilder
     */
    private $imageBuilder;

    public function __construct(
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        FilterBuilder $filterBuilder,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Framework\Data\CollectionFactory $collectionFactory,
        \Magento\Framework\DataObjectFactory $dataObjectFactory,
        \Magento\Catalog\Block\Product\ImageBuilder $imageBuilder
    ) {
        $this->productRepository = $productRepository;
        $this->filterBuilder = $filterBuilder;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->collectionFactory = $collectionFactory;
        $this->dataObjectFactory = $dataObjectFactory;
        $this->imageBuilder = $imageBuilder;
    }

    /**
     * @param array $productIds
     * @return \Magento\Framework\Data\Collection
     * @throws \Exception
     */
    public function getProductCollectionWithImage(array $productIds)
    {
        $filter = $this->filterBuilder->setField('entity_id')
            ->setConditionType('in')
            ->setValue($productIds)
            ->create();

        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilters([$filter])
            ->create();

        $result = $this->productRepository->getList($searchCriteria);

        $parsedCollection = $this->collectionFactory->create();

        if ($result->getTotalCount() > 0) {
            foreach ($result->getItems() as $item) {
                $item->setImage($this->buildImage($item));
                $parsedCollection->addItem($item);
            }
        }

        return $parsedCollection;
    }

    private function buildImage(\Magento\Catalog\Api\Data\ProductInterface $item)
    {
        return $this->imageBuilder->create($item, 'category_page_grid')->toHtml();
    }
}
