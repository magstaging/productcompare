<?php

namespace Mbs\ProductCompare\Model;

use Magento\Catalog\Model\ResourceModel\Eav\Attribute;
use Magento\Catalog\Model\ResourceModel\Product\Compare\Item\Collection;

class ProductAttributesFilter
{
    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory
     */
    private $collectionFactory;

    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory $collectionFactory
    ) {
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * @param $groupId
     * @param $attributes
     * @return \Magento\Catalog\Model\ResourceModel\Product\Attribute\Collection
     */
    public function getAttributesListInGroup($groupId, $attributes)
    {
        $attributeIds = [];
        foreach ($attributes as $attribute) {
            $attributeIds[] = $attribute->getId();
        }

        $collection = $this->collectionFactory->create();
        $collection->addFieldToFilter('main_table.attribute_id', ['in' => $attributeIds]);

        $collection->getSelect()->join(
            ['entity_table' => $collection->getTable('eav_entity_attribute')],
            'entity_table.attribute_id=main_table.attribute_id',
            []
        )->where('entity_table.attribute_group_id=?', $groupId);

        return $collection;
    }

    public function hasItemsNonEmptyAttributeValues(Attribute $attribute, Collection $items)
    {
        $result = false;

        foreach ($items as $item) {
            if ($item->getData($attribute->getAttributeCode())!= '') {
                $result = true;
            }

            if ($result) {
                break;
            }
        }

        return $result;
    }

    /**
     * @param Attribute $attribute
     * @param Collection $items
     * @return int|void
     */
    public function countDifferentItemsAttributeValues(Attribute $attribute, Collection $items)
    {
        $attributeItemValues = [];

        foreach ($items as $item) {
            if ($item->getData($attribute->getAttributeCode())!= '') {
                $attributeItemValues[$item->getData($attribute->getAttributeCode())] = '';
            }
        }

        return count($attributeItemValues);
    }
}
