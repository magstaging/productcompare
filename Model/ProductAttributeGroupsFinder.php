<?php

namespace Mbs\ProductCompare\Model;

use Magento\Catalog\Model\ResourceModel\Product\Compare\Item\Collection;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\Group\CollectionFactory;

class ProductAttributeGroupsFinder
{
    /**
     * @var CollectionFactory
     */
    private $_groupCollection;
    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory
     */
    private $attributeCollectionFactory;

    public function __construct(
        CollectionFactory $_groupCollection,
        \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory $attributeCollectionFactory
    ) {
        $this->_groupCollection = $_groupCollection;
        $this->attributeCollectionFactory = $attributeCollectionFactory;
    }

    /**
     * @param Collection $product
     * @return \Magento\Eav\Model\ResourceModel\Entity\Attribute\Group\Collection
     */
    public function findGroupListForProduct(Collection $products, $attributes)
    {
        $attributeSetIds = [];
        foreach ($products as $product) {
            $attributeSetIds = $product->getAttributeSetId();
        }

        $groupCollection = $this->_groupCollection->create();
        $groupCollection->addFieldToFilter('main_table.attribute_set_id', ['in' => $attributeSetIds]);

        $attributeIds = [];
        foreach ($attributes as $attribute) {
            $attributeIds[] = $attribute->getId();
        }

        $groupCollection->getSelect()->join(
            ['entity_table' => $groupCollection->getTable('eav_entity_attribute')],
            sprintf('entity_table.attribute_group_id=main_table.attribute_group_id and entity_table.attribute_id in (%s)', implode(',', $attributeIds)),
            []
        )->group('main_table.attribute_group_id');

        return $groupCollection;
    }
}