<?php

namespace Mbs\ProductCompare\Model;

use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\SearchCriteriaBuilder;

class CompareSidebarProductHandler
{
    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    private $productRepository;
    /**
     * @var FilterBuilder
     */
    private $filterBuilder;
    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;
    /**
     * @var \Magento\Framework\Data\CollectionFactory
     */
    private $collectionFactory;
    /**
     * @var \Magento\Framework\DataObjectFactory
     */
    private $dataObjectFactory;
    /**
     * @var \Magento\Catalog\Model\Product\Compare\ListCompare
     */
    private $listCompare;
    /**
     * @var \Magento\Catalog\Helper\Product\Compare
     */
    private $compare;

    public function __construct(
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        FilterBuilder $filterBuilder,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Framework\Data\CollectionFactory $collectionFactory,
        \Magento\Framework\DataObjectFactory $dataObjectFactory,
        \Magento\Catalog\Model\Product\Compare\ListCompare $listCompare,
        \Magento\Catalog\Helper\Product\Compare $compare
    ) {
        $this->productRepository = $productRepository;
        $this->filterBuilder = $filterBuilder;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->collectionFactory = $collectionFactory;
        $this->dataObjectFactory = $dataObjectFactory;
        $this->listCompare = $listCompare;
        $this->compare = $compare;
    }

    public function addProductListToComparisonWidget(array $productIds)
    {
        $this->listCompare->addProducts($productIds);

        $this->compare->calculate();
    }
}