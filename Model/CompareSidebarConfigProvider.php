<?php

namespace Mbs\ProductCompare\Model;

use Magento\Framework\UrlInterface;

class CompareSidebarConfigProvider
{
    /**
     * @var UrlInterface
     */
    private $urlBuilder;

    public function __construct(
        UrlInterface $urlBuilder
    ) {
        $this->urlBuilder = $urlBuilder;
    }

    public function getCompareSidebarRefreshUrl()
    {
        return $this->urlBuilder->getUrl('mbsproductcompare/refresh');
    }
}
